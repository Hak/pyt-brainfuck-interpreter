from exceptions import PNGWrongHeaderError
from exceptions import PNGNotImplementedError
import time
import zlib

def its_braincopter(image):
    for row in image:
        for pixel in row:
            if (pixel != (255,0,0) and pixel != (128,0,0) and pixel != (0,255,0) and pixel != (0,128,0) and pixel != (0,0,255) and pixel != (0,0,128) and pixel != (255,255,0) and pixel != (128,128,0) and pixel != (0,255,255) and pixel != (0,128,128) and pixel != (0,0,0)):
                return True
    return False

def paeth(a, b, c):
    p = a + b - c
    pa = abs(p - a)
    pb = abs(p - b)
    pc = abs(p - c)
    if pa <= pb and pa <= pc:
        return a
    elif pb <= pc:
        return b
    else:
        return c

def defilter(image, filters):
    for y, f in enumerate(filters):
        if (f == 1):
            for x, pixel in enumerate(image[y]):
                if (x > 0):
                    image[y][x] = ((image[y][x][0] + image[y][x - 1][0]) % 256, (image[y][x][1] + image[y][x - 1][1]) % 256, (image[y][x][2] + image[y][x - 1][2]) % 256)
        elif (f == 2):
            for x, pixel in enumerate(image[y]):
                if (y > 0):
                    image[y][x] = ((image[y][x][0] + image[y - 1][x][0]) % 256, (image[y][x][1] + image[y - 1][x][1]) % 256, (image[y][x][2] + image[y - 1][x][2]) % 256)
        elif (f == 3):
            for x, pixel in enumerate(image[y]):
                if (y > 0 and x > 0):
                    image[y][x] = (((image[y][x][0] + image[y][x - 1][0] + image[y - 1][x][0]) // 2) % 256, ((image[y][x][1] + image[y][x - 1][1] + image[y - 1][x][1]) // 2) % 256, ((image[y][x][2] + image[y][x - 1][2] + image[y - 1][x][2]) // 2) % 256)
        elif (f == 4):
            for x, pixel in enumerate(image[y]):
                if (y > 0 and x > 0):
                    image[y][x] = ((image[y][x][0] + paeth(image[y][x - 1][0], image[y - 1][x][0], image[y - 1][x - 1][0])) % 256, (image[y][x][1] + paeth(image[y][x - 1][1], image[y - 1][x][1], image[y - 1][x - 1][1])) % 256, (image[y][x][2] + paeth(image[y][x - 1][2], image[y - 1][x][2], image[y - 1][x - 1][2])) % 256)
                elif (x > 0):
                    image[y][x] = ((image[y][x][0] + paeth(image[y][x - 1][0], 0, 0)) % 256, (image[y][x][1] + paeth(image[y][x - 1][1], 0, 0)) % 256, (image[y][x][2] + paeth(image[y][x - 1][2], 0, 0)) % 256)
                elif (y > 0):
                    image[y][x] = ((image[y][x][0] + paeth(0, image[y - 1][x][0], 0)) % 256, (image[y][x][1] + paeth(0, image[y - 1][x][1], 0)) % 256, (image[y][x][2] + paeth(0, image[y - 1][x][2], 0)) % 256)
    return image

def get_data_from_png(path):
    imageHeight = imageWidth = 0
    imageCompressedData = b""
    with open(path, mode='rb') as file:
        if (file.read(8) != b"\x89\x50\x4E\x47\x0D\x0A\x1A\x0A"):
            raise PNGWrongHeaderError
        while True:
            chunkLength = int.from_bytes(file.read(4), byteorder='big')
            chunkType = file.read(4)
            if (chunkType == b"IEND"):
                imageData = zlib.decompress(imageCompressedData)
                pixel = ()
                raw = []
                image = []
                filters = []
                for i, byte in enumerate(imageData):
                    if (i != 0):
                        if (i % (imageWidth * 3 + 1) == 0):
                            filters.append(byte)
                            image.append(raw)
                            raw = []
                        else:
                            pixel = pixel + (byte,)
                            if (len(pixel) == 3):
                                raw.append(pixel)
                                pixel = ()
                    else:
                        filters.append(byte)
                image.append(raw)
                return defilter(image, filters)   
            elif (chunkType == b"IHDR"):
                imageWidth = int.from_bytes(file.read(4), byteorder='big')
                imageHeight = int.from_bytes(file.read(4), byteorder='big')
                if (file.read(5) != b"\x08\x02\x00\x00\x00"):
                    raise PNGNotImplementedError
                file.read(4)
            elif (chunkType == b"IDAT"):
                imageCompressedData += file.read(chunkLength)
                tmp = file.read(4)
            else:
                tmp = file.read(chunkLength + 4)

def get_brainfuck_from_brainloller(image):
    width = 0
    height = len(image)
    if (height != 0):
        width = len(image[0])
    yIndex = xIndex = yDirection = 0
    xDirection = 1
    brainfuck = ""  
    while (0 <= xIndex < width and 0 <= yIndex < height):
        if (image[yIndex][xIndex] == (255,0,0)):
            brainfuck += '>'
        elif (image[yIndex][xIndex] == (128,0,0)):
            brainfuck += '<'
        elif (image[yIndex][xIndex] == (0,255,0)):
            brainfuck += '+'
        elif (image[yIndex][xIndex] == (0,128,0)):
            brainfuck += '-'
        elif (image[yIndex][xIndex] == (0,0,255)):
            brainfuck += '.'
        elif (image[yIndex][xIndex] == (0,0,128)):
            brainfuck += ','
        elif (image[yIndex][xIndex] == (255,255,0)):
            brainfuck += '['
        elif (image[yIndex][xIndex] == (128,128,0)):
            brainfuck += ']'
        elif (image[yIndex][xIndex] == (0,255,255)):
            if (xDirection == 1 and yDirection == 0):
                xDirection = 0
                yDirection = 1
            elif (xDirection == 0 and yDirection == 1):
                xDirection = -1
                yDirection = 0
            elif (xDirection == -1 and yDirection == 0):
                xDirection = 0
                yDirection = -1
            else:
                xDirection = 1
                yDirection = 0
        elif (image[yIndex][xIndex] == (0,128,128)):
            if (xDirection == 1 and yDirection == 0):
                xDirection = 0
                yDirection = -1
            elif (xDirection == 0 and yDirection == 1):
                xDirection = 1
                yDirection = 0
            elif (xDirection == -1 and yDirection == 0):
                xDirection = 0
                yDirection = 1
            else:
                xDirection = -1
                yDirection = 0
        else:
            pass
        xIndex += xDirection
        yIndex += yDirection
    return brainfuck

def get_brainfuck_from_braincopter(image):
    width = 0
    height = len(image)
    if (height != 0):
        width = len(image[0])
    yIndex = xIndex = yDirection = 0
    xDirection = 1
    brainfuck = ""  
    while (0 <= xIndex < width and 0 <= yIndex < height):
        color = (9 * image[yIndex][xIndex][0] + 3 * image[yIndex][xIndex][1] + image[yIndex][xIndex][2]) % 11
        if (color == 0):
            brainfuck += '>'
        elif (color == 1):
            brainfuck += '<'
        elif (color == 2):
            brainfuck += '+'
        elif (color == 3):
            brainfuck += '-'
        elif (color == 4):
            brainfuck += '.'
        elif (color == 5):
            brainfuck += ','
        elif (color == 6):
            brainfuck += '['
        elif (color == 7):
            brainfuck += ']'
        elif (color == 8):
            if (xDirection == 1 and yDirection == 0):
                xDirection = 0
                yDirection = 1
            elif (xDirection == 0 and yDirection == 1):
                xDirection = -1
                yDirection = 0
            elif (xDirection == -1 and yDirection == 0):
                xDirection = 0
                yDirection = -1
            else:
                xDirection = 1
                yDirection = 0
        elif (color == 9):
            if (xDirection == 1 and yDirection == 0):
                xDirection = 0
                yDirection = -1
            elif (xDirection == 0 and yDirection == 1):
                xDirection = 1
                yDirection = 0
            elif (xDirection == -1 and yDirection == 0):
                xDirection = 0
                yDirection = 1
            else:
                xDirection = -1
                yDirection = 0
        else:
            pass
        xIndex += xDirection
        yIndex += yDirection
    return brainfuck

class SimpleBrainfuck:
    def __init__(self, program, task, data, dataPointer, test, image):
        self.test = test
        self.debugFileNumber = 1
        self.program = program
        self._remove_comments()
        self.task = task
        self.data = self._byte_string_to_data(data)
        self.programPointer = 0
        self.taskPointer = 0
        self.dataPointer = dataPointer
        self.loops = {}
        self._find_loops()
        self.output = b""
        self.image = image
                
    def execute_program(self):
        while(self.programPointer < len(self.program)):
            self._execute_instruction()
        if (self.test == True):
            self._create_debug_file()
    
    def create_braincopter_image(self, path, image): #It doesnt work
        buffer = b"\x89\x50\x4E\x47\x0D\x0A\x1A\x0A" + (13).to_bytes(4, byteorder='big')
        tmp = b"IHDR" + (len(image[0])).to_bytes(4, byteorder='big') + (len(image)).to_bytes(4, byteorder='big') + b"\x08\x02\x00\x00\x00"
        buffer += tmp + (zlib.crc32(tmp)).to_bytes(4, byteorder='big')
        row = b''
        imageInBytes = b''
        i = j = x = 0
        while (i < len(image[0]) and j < len(image)):
            if (i + j == 0):
                row += b'\x00' + self._get_braincopter_pixel(self.program[x], image[0][0])
                i += 1
            elif (i == 0 and j % 2 == 0):
                row += b'\x00' + self._get_braincopter_pixel('l', image[j][i])
                i += 1
            elif (i == len(image[0]) - 1 and j % 2 == 0):
                row += self._get_braincopter_pixel('p', image[j][i])
                imageInBytes += row
                row = b''
                j += 1
            elif (j % 2 == 0):
                if (x < len(self.program)):
                    row += self._get_braincopter_pixel(self.program[x], image[j][i])
                else:
                    row += self._get_braincopter_pixel('', image[j][i])
                i += 1
            elif (i == 0 and j % 2 == 1):
                row = b'\x00' + self._get_braincopter_pixel('l', image[j][i]) + row
                imageInBytes += row
                row = b''
                j += 1
            elif (i == len(image[0]) - 1 and j % 2 == 1):
                row = self._get_braincopter_pixel('p', image[j][i]) + row
                i -= 1
            elif (j % 2 == 1):
                if (x < len(self.program)):
                    row = self._get_braincopter_pixel(self.program[x], image[j][i]) + row
                else:
                    row = self._get_braincopter_pixel('', image[j][i]) + row
                i -= 1
            x += 1
        imageInBytes = zlib.compress(imageInBytes)
        buffer += (len(imageInBytes)).to_bytes(4, byteorder='big')
        tmp = b"IDAT" + imageInBytes
        buffer += tmp + (zlib.crc32(tmp)).to_bytes(4, byteorder='big')
        buffer += b'\x00\x00\x00\x00' + b'IEND' + b'\xaeB`\x82'
        with open(path, 'wb') as file:
            file.write(buffer)
        
    def create_brainloller_image(self, path):
        program = self.program
        if (len(program) % 2 == 1):
            program += "e"
        buffer = b"\x89\x50\x4E\x47\x0D\x0A\x1A\x0A" + (13).to_bytes(4, byteorder='big')
        tmp = b"IHDR" + (4).to_bytes(4, byteorder='big') + (len(program)//2).to_bytes(4, byteorder='big') + b"\x08\x02\x00\x00\x00"
        buffer += tmp + (zlib.crc32(tmp)).to_bytes(4, byteorder='big')
        imageInBytes = b""
        iterator = iter(program)
        i = 0
        for x in iterator:
            if (i == 0):
                imageInBytes += b'\x00\x00\x00\x00'
                imageInBytes += self._get_brainloller_pixel(x)
                imageInBytes += self._get_brainloller_pixel(next(iterator))
                imageInBytes += b'\x00\xff\xff'
            elif (i % 2 == 0):
                imageInBytes += b'\x00\x00\x80\x80'
                imageInBytes += self._get_brainloller_pixel(x)
                imageInBytes += self._get_brainloller_pixel(next(iterator))
                imageInBytes += b'\x00\xff\xff'
            else:
                imageInBytes += b'\x00\x00\x80\x80'
                imageInBytes += self._get_brainloller_pixel(next(iterator))
                imageInBytes += self._get_brainloller_pixel(x)
                imageInBytes += b'\x00\xff\xff'
            i += 1
        imageInBytes = zlib.compress(imageInBytes)
        buffer += (len(imageInBytes)).to_bytes(4, byteorder='big')
        tmp = b"IDAT" + imageInBytes
        buffer += tmp + (zlib.crc32(tmp)).to_bytes(4, byteorder='big')
        buffer += b'\x00\x00\x00\x00' + b'IEND' + b'\xaeB`\x82'
        with open(path, 'wb') as file:
            file.write(buffer)

    def _execute_instruction(self):
        if (self.program[self.programPointer] == '>'):
            self.dataPointer += 1
            if (self.dataPointer == len(self.data)):
                self.data.append(0)
        elif (self.program[self.programPointer] == '<'):
            if (self.dataPointer > 0):
                self.dataPointer -= 1
        elif (self.program[self.programPointer] == '+'):
            self.data[self.dataPointer] = (self.data[self.dataPointer] + 1) % 256
        elif (self.program[self.programPointer] == '-'):
            self.data[self.dataPointer] = (self.data[self.dataPointer] + 255) % 256
        elif (self.program[self.programPointer] == '.'):
            self.output += (self.data[self.dataPointer]).to_bytes(1, byteorder='big')
            print((self.data[self.dataPointer]).to_bytes(1, byteorder='big').decode("utf-8"), end='')
        elif (self.program[self.programPointer] == ','):
            self.data[self.dataPointer] = ord(self.task[self.taskPointer])
            self.taskPointer += 1
        elif (self.program[self.programPointer] == '['):
            if (self.data[self.dataPointer] == 0):
                self.programPointer = self.loops[self.programPointer]      
        elif (self.program[self.programPointer] == ']'):
            if (self.data[self.dataPointer] != 0):
                self.programPointer = self.loops[self.programPointer]
        elif (self.program[self.programPointer] == '#'):
            self._create_debug_file()
        else:
            pass
        self.programPointer += 1

    def _get_braincopter_pixel(self, code, pixel): #It doesnt work
        result = bytes([pixel[0]]) + bytes([pixel[1]])
        if (code == '>'):
            return bytes([0]) + bytes([0]) + bytes([0])
        elif (code == '<'):
            return bytes([0]) + bytes([0]) + bytes([11+1])
        elif (code == '+'):
            return bytes([0]) + bytes([0]) + bytes([22+2])
        elif (code == '-'):
            return bytes([0]) + bytes([0]) + bytes([33+3])
        elif (code == '.'):
            return bytes([0]) + bytes([0]) + bytes([44+4])
        elif (code == ','):
            return bytes([0]) + bytes([0]) + bytes([55+5])
        elif (code == '['):
            return bytes([0]) + bytes([0]) + bytes([66+6])
        elif (code == ']'):
            return bytes([0]) + bytes([0]) + bytes([77+7])
        elif (code == 'p'):
            return bytes([0]) + bytes([0]) + bytes([88+8])
        elif (code == 'l'):
            return bytes([0]) + bytes([0]) + bytes([99+9])
        else:
            return bytes([0]) + bytes([0]) + bytes([110+10])
    
    def _get_brainloller_pixel(self, code):
        if (code == '>'):
            return b'\xff\x00\x00'
        elif (code == '<'):
            return b'\x80\x00\x00'
        elif (code == '+'):
            return b'\x00\xff\x00'
        elif (code == '-'):
            return b'\x00\x80\x00'
        elif (code == '.'):
            return b'\x00\x00\xff'
        elif (code == ','):
            return b'\x00\x00\x80'
        elif (code == '['):
            return b'\xff\xff\x00'
        elif (code == ']'):
            return b'\x80\x80\x00'
        else:
            return b'\x00\x00\x00'

    def _fit(self, number):
        while (number > 255):
            number -= 11
        return number
    
    def _create_debug_file(self):
        zero = ""
        if (self.debugFileNumber < 10):
            zero = "0"
        with open("debug_" + zero + str(self.debugFileNumber) + ".log", 'w', newline='') as file:
            file.write("# program data\n")
            file.write(self.program + "\n\n")
            file.write("# memory\n")
            file.write(str(self._data_to_byte_string()) + "\n\n")
            file.write("# memory pointer\n")
            file.write(str(self.dataPointer) + "\n\n")
            file.write("# output\n")
            file.write(str(self.output) + "\n\n")
            if (self.image != None):
                file.write("# RGB input\n[\n")
                for row in self.image:
                    file.write("    " + str(row) + ",\n")
                file.write("]\n\n")
        self.debugFileNumber += 1

    def _find_loops(self):
        stack = []
        for i, character in enumerate(self.program):
            if (character == "["):
                stack.append(i)
            elif (character == "]"):
                tmp = stack.pop()
                self.loops[i] = tmp
                self.loops[tmp] = i
                
    def _remove_comments(self):
        removedComments = ""
        for char in self.program:
            if (char == '>' or char == '<' or char == '+' or char == '-' or char == '.' or char == ',' or char == '[' or char == ']' or char == '#'):
                removedComments += char
        self.program = removedComments
        
    def _data_to_byte_string(self):
        data = b""
        for byte in self.data:
            data += (byte).to_bytes(1, byteorder='big')
        return data
    
    def _byte_string_to_data(self, byteString):
        data = []
        for byte in byteString:
            data.append(byte)
        self.data = data
        return data
