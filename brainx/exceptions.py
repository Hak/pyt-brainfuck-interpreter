class PNGWrongHeaderError(Exception):
    def __init__(self, code = 4):
        self.code = code
    def __str__(self):
        return str(self.__class__)
    def getCode(self):
        return self.code

class PNGNotImplementedError(Exception):
    def __init__(self, code = 8):
        self.code = code
    def __str__(self):
        return str(self.__class__)
    def getCode(self):
        return self.code
