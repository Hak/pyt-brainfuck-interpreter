#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import binascii
import sys, traceback
from exceptions import PNGWrongHeaderError
from exceptions import PNGNotImplementedError
from argparse import ArgumentParser
from argparse import ArgumentTypeError
from brainfuck import SimpleBrainfuck
from brainfuck import get_data_from_png
from brainfuck import its_braincopter
from brainfuck import get_brainfuck_from_brainloller
from brainfuck import get_brainfuck_from_braincopter

def input_string_to_byte_array(string):
    tmp = string[2:(len(string)-1)].replace("\\x", "").replace("\\r", "0d").replace("\\n", "0a").replace("\\t", "09")
    result = ""
    for i in tmp:
        if (ord(i) > 102 or (ord(i) < 97 and ord(i) > 57) or ord(i) < 47):
            result += str(hex(ord(i)))
        elif ((ord(i) <= 102 and ord(i) >= 97) or (ord(i) <= 57 and ord(i) >= 48)):
            result += i
    return binascii.unhexlify(result.replace("0x", ""))

parser = ArgumentParser()
parser.add_argument('--version', action='version', version='1.1')

group = parser.add_mutually_exclusive_group()
group.add_argument('file', nargs='?')
group.add_argument("--lc2f", nargs='+', default=None, help="překlad z obrázku na výstup nebo do textového souboru")
group.add_argument("--f2lc", action='store_true', default=False, help="překlad z textového vstupu do PNG-obrázku")

parser.add_argument("-m", "--memory", default="b'00'", help="počáteční stav paměti interpretru v podobě pythoního bajtového řetězce")
parser.add_argument("-p", "--pointer", type=int, default=0, help="počáteční ukazatel do paměti interpretru v podobě nezáporného celého čísla")
parser.add_argument("-t", "--test", action="store_true", default=False, help="aktivace testovacích a ladicích výpisů")
parser.add_argument("-i", "--input", nargs='+', help="input file for mode --f2lc")
parser.add_argument("-o", "--output", help="output file for mode --f2lc")
args = parser.parse_args()

if (args.file != None): #Basic mode of program (run the brainfuck/brainloller/braincopter)
    memory = input_string_to_byte_array(args.memory)
    interpreter = None
    if (args.file == None):
        tmp = input().split('!', 1)
        tmp.append("")
        interpreter = SimpleBrainfuck(tmp[0], tmp[1], memory, args.pointer, args.test, None)
    elif (args.file.startswith('"') and args.file.endswith('"')):
        tmp = args.file[1:(len(args.file)-1)].split('!', 1)
        tmp.append("")
        interpreter = SimpleBrainfuck(tmp[0], tmp[1], memory, args.pointer, args.test, None)
    elif (args.file.endswith('.b')):
        with open(args.file, encoding='utf-8') as soubor:
            tmp = soubor.read().split('!', 1)
            tmp.append("") 
            interpreter = SimpleBrainfuck(tmp[0], tmp[1], memory, args.pointer, args.test, None)
    elif (args.file.endswith('.png')):
        try:
            image = get_data_from_png(args.file)
            if (its_braincopter(image) == True):
                interpreter = SimpleBrainfuck(get_brainfuck_from_braincopter(image), "", memory, args.pointer, args.test, image)
            else:
                interpreter = SimpleBrainfuck(get_brainfuck_from_brainloller(image), "", memory, args.pointer, args.test, image)
        except (PNGWrongHeaderError):
            traceback.print_exc(file=sys.stderr)
            exit(4)
        except (PNGNotImplementedError):
            traceback.print_exc(file=sys.stderr)
            exit(8)
    else:
        try:
            raise PNGWrongHeaderError
        except (PNGWrongHeaderError):
            traceback.print_exc(file=sys.stderr)
            exit(4)
    interpreter.execute_program()
elif (args.f2lc == True): #Překlad z textového vstupu do PNG-obrázku
    if (len(args.input) == 1 and args.output != None):
        with open(args.input[0], encoding='utf-8') as soubor:
            tmp = soubor.read().split('!', 1)
            tmp.append("") 
            interpreter = SimpleBrainfuck(tmp[0], tmp[1], b'', args.pointer, args.test, None)
            interpreter.create_brainloller_image(args.output)
    elif (len(args.input) == 2 and args.output != None): #It doesnt work
       with open(args.input[0], encoding='utf-8') as soubor:
            tmp = soubor.read().split('!', 1)
            tmp.append("") 
            interpreter = SimpleBrainfuck(tmp[0], tmp[1], b'', args.pointer, args.test, None)
            interpreter.create_braincopter_image(args.output, get_data_from_png(args.input[1]))
    else:
        sys.stderr.write("brainx: error: argument --f2lc, -i, -o: the format is not right")
        exit(1)
else: #Překlad z PNG-obrázku na výstup nebo do textového souboru
    if (len(args.lc2f) == 1):
        try:
            image = get_data_from_png(args.lc2f[0])
            if (its_braincopter(image) == True):
                print(get_brainfuck_from_braincopter(image), end='\r')
            else:
                print(get_brainfuck_from_brainloller(image), end='\r')
        except (PNGWrongHeaderError):
            traceback.print_exc(file=sys.stderr)
            exit(4)
        except (PNGNotImplementedError):
            traceback.print_exc(file=sys.stderr)
            exit(8)
    elif (len(args.lc2f) == 2):
        try:
            image = get_data_from_png(args.lc2f[0])
            if (its_braincopter(image) == True):
                with open(args.lc2f[1], 'w', encoding='utf-8', newline='') as file: 
                    file.write(get_brainfuck_from_braincopter(image))
            else:
                with open(args.lc2f[1], 'w', encoding='utf-8', newline='') as file: 
                    file.write(get_brainfuck_from_brainloller(image))
        except (PNGWrongHeaderError):
            traceback.print_exc(file=sys.stderr)
            exit(4)
        except (PNGNotImplementedError):
            traceback.print_exc(file=sys.stderr)
            exit(8)
    else:
        sys.stderr.write("brainx: error: argument --lc2f: expected 1 or 2 arguments")
        exit(1)
